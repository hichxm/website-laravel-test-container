<?php

function docker_secret(string $name, $default = null)
{
    if($content = file_get_contents('/run/secrets/' . $name))
        return trim($content);
    
    return $default;
}